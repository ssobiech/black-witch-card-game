package pl.sobiech.blackwitch;

import pl.sobiech.blackwitch.card.Card;
import pl.sobiech.blackwitch.player.Player;
import pl.sobiech.blackwitch.player.PlayerList;

class Turn {

    private static final int MAX_DISCARDED_PAIRS_PER_TURN = 2;
    private static final int MAX_PICKED_CARDS_PER_TURN = 1;

    private int discardedCount = 0;
    private int pickedCount = 0;

    private final UI ui;

    Turn(UI ui) {
        this.ui = ui;
    }

    void execute(PlayerList players, Player chosenPlayer) {
        ui.out(chosenPlayer.toString());

        String isMoveFinished = "n";
        while (isMoveFinished.equalsIgnoreCase("n")) {
            ui.printPlayerMoveInfo();
            int moveNumber = ui.readNumber("Type in the number of the move: ");

            switch (moveNumber) {
                case 1:
                    handleDiscardingPairs(players, chosenPlayer);
                    break;
                case 2:
                    handlePickingCardsFromPreviousPlayer(players, chosenPlayer);
                    break;
                case 3:
                    handleLeaveTheGame(players, chosenPlayer);
                    break;
                default:
                    ui.out("Wrong option!\n");
            }

            isMoveFinished = ui.readText("Finished your turn? (y/n): ");
        }
    }

    private void handleDiscardingPairs(PlayerList players, Player chosenPlayer) {
        if (discardedCount < MAX_DISCARDED_PAIRS_PER_TURN) {
            discardPair(chosenPlayer);
            removePlayerIfNoCards(players, chosenPlayer);
        } else {
            ui.out("You can discard only 2 pairs in 1 turn!\n");
        }
    }

    private void discardPair(Player chosenPlayer) {
        boolean isPairDiscarded = false;

        while (!isPairDiscarded) {
            final int firstCardIndex = ui.readNumber("Type in number of first card to discard: ") - 1;
            final int secondCardIndex = ui.readNumber("Type in number of second card to discard: ") - 1;

            if (firstCardIndex == secondCardIndex) {
                ui.out("You cannot pick the same card!\n\n");
                break;
            }

            Card firstCard = chosenPlayer.getCard(firstCardIndex);
            Card secondCard = chosenPlayer.getCard(secondCardIndex);

            isPairDiscarded = chosenPlayer.discardPair(firstCard, secondCard);

            if (isPairDiscarded) {
                discardedCount++;
            }
        }

        displayCurrentCards(chosenPlayer);
    }

    private void handlePickingCardsFromPreviousPlayer(PlayerList players, Player chosenPlayer) {
        if (pickedCount < MAX_PICKED_CARDS_PER_TURN) {
            Player previousPlayer = players.previous();
            pickCardFromPreviousPlayer(previousPlayer, chosenPlayer);
            removePlayerIfNoCards(players, previousPlayer);
            pickedCount++;
        } else {
            ui.out("You cannot pick anymore cards in this turn!\n");
        }
    }

    private void pickCardFromPreviousPlayer(Player previousPlayer, Player chosenPlayer) {
        int cardsOfPreviousPlayer = previousPlayer.getQuantityOfCards();
        if (cardsOfPreviousPlayer > 0) {
            final int cardNumber =
                    ui.readNumber("Choose number of the card you want to pick(1-" + cardsOfPreviousPlayer + "): ");
            final int cardIndex = cardNumber - 1;
            Card cardFromPreviousPlayer = previousPlayer.getCard(cardIndex);
            chosenPlayer.addCard(cardFromPreviousPlayer);
            previousPlayer.removeCard(cardIndex);
            displayCurrentCards(chosenPlayer);
        } else {
            ui.out("Previous player has no more cards.\n");
        }
    }

    private void removePlayerIfNoCards(PlayerList players, Player player) {
        if (!player.hasCards()) {
            ui.printPlayerWinsMsg(player.getId());
            players.remove(player);
        }
    }

    private void displayCurrentCards(Player choosenPlayer) {
        ui.out("Your deck looks like this now:\n");
        choosenPlayer.displayCards();
    }

    private void handleLeaveTheGame(PlayerList players, Player chosenPlayer) {
        players.remove(chosenPlayer);
    }
}
