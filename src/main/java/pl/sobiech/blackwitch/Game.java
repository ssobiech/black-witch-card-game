package pl.sobiech.blackwitch;

import pl.sobiech.blackwitch.player.Player;
import pl.sobiech.blackwitch.player.PlayerList;

class Game {

    private final UI ui;

    Game() {
        this.ui = new UI();
    }

    void run() {
        ui.printWelcomeMessage();

        PlayerList players = new PlayerList(ui.readNumberOfPlayers(), ui.readNumberOfCardsInHand());
        ui.printPlayersHaveCardsMsg();

        while (players.hasNext()) {
            Player currentPlayer = players.next();

            if (onePlayerRemains(players, currentPlayer)) break;

            if (currentPlayer.hasCards()) {
                new Turn(ui).execute(players, currentPlayer);
            } else {
                ui.printPlayerWinsMsg(currentPlayer.getId());
                players.remove();
            }
        }
    }

    private boolean onePlayerRemains(PlayerList players, Player currentPlayer) {
        boolean isOnePlayerRemains = false;
        if (players.getQuantityOfPlayers() == 1) {
            ui.printPlayerLostMsg(currentPlayer.getId());
            isOnePlayerRemains = true;
        }
        return isOnePlayerRemains;
    }
}
