package pl.sobiech.blackwitch.card;

enum CardFigure {

    ACE("A"),
    KING("K"),
    QUEEN("Q"),
    JACK("J"),
    TEN("10"),
    NINE("9"),
    EIGHT("8"),
    SEVEN("7"),
    SIX("6"),
    FIVE("5"),
    FOUR("4"),
    THREE("3"),
    TWO("2");

    private String value;

    CardFigure(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
