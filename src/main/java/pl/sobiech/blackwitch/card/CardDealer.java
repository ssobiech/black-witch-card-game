package pl.sobiech.blackwitch.card;


import java.util.ArrayList;
import java.util.List;

public class CardDealer {

    private CardDeck cardDeck;

    public CardDealer() {
        cardDeck = new CardDeck();
    }

    public void shuffleCards() {
        cardDeck.shuffleCards();
    }

    public List<Card> dealCards(int quantityOfCards) {
        List<Card> dealtCards = new ArrayList<>();

        while(dealtCards.size() < quantityOfCards && cardDeck.iterator().hasNext()) {
            Card pickedCard = cardDeck.iterator().next();
            dealtCards.add(pickedCard);
            cardDeck.removeCard(pickedCard);
        }

        return dealtCards;
    }
}
