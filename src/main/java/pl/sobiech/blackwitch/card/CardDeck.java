package pl.sobiech.blackwitch.card;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

class CardDeck implements Iterable<Card> {

    private List<Card> cards;

    CardDeck() {
        DeckCreator deckCreator = new DeckCreator();
        this.cards = deckCreator.createDeck();
    }

    void shuffleCards() {
        Collections.shuffle(cards);
    }

    void removeCard(Card card) {
        cards.remove(card);
    }

    @Override
    public Iterator<Card> iterator() {
        return cards.iterator();
    }

    @Override
    public void forEach(Consumer<? super Card> action) {
        cards.forEach(action);
    }
}
