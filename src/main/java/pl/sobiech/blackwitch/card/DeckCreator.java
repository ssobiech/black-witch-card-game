package pl.sobiech.blackwitch.card;

import java.util.ArrayList;
import java.util.List;

class DeckCreator {

    List<Card> createDeck() {
        final List<Card> deck = new ArrayList<>();

        for (CardFigure cardFigure : CardFigure.values()) {
                for (CardShape cardShape : CardShape.values()) {
                    Card numberCard = new Card(cardFigure, cardShape);
                    deck.add(numberCard);
                }
        }

        Card queenOfSpades = new Card(CardFigure.QUEEN, CardShape.SPADE);
        deck.remove(queenOfSpades);

        return deck;
    }
}
