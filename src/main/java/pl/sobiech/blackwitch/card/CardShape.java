package pl.sobiech.blackwitch.card;

enum CardShape {

    CLUB("\u2663"), DIAMOND("\u2666"), HEART("\u2665"), SPADE("\u2660");

    private String symbol;

    CardShape(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return symbol;
    }
}
