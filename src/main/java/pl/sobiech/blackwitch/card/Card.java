package pl.sobiech.blackwitch.card;

import java.util.Objects;

public class Card implements Comparable<Card> {

    private CardFigure cardFigure;
    private CardShape cardShape;

    Card(CardFigure cardFigure, CardShape cardShape) {
        this.cardFigure = cardFigure;
        this.cardShape = cardShape;
    }

    @Override
    public int compareTo(Card card) {
        if (this.cardFigure.equals(card.cardFigure)
                || this.cardShape.equals(card.cardShape)) {
            return 1;
        } else return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return cardFigure == card.cardFigure &&
                cardShape == card.cardShape;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardFigure, cardShape);
    }

    @Override
    public String toString() {
        return cardFigure.toString() + cardShape.toString();
    }
}
