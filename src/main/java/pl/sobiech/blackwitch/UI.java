package pl.sobiech.blackwitch;

import java.util.Scanner;
import java.util.function.Consumer;

class UI {

    private static final int DEFAULT_NUMBER_OF_CARDS = 5;
    private static final String IS_NUMBER_PATTERN = "[0-9]+";

    private final Consumer<Object> out;
    private final Scanner in;

    UI() {
        out = System.out::print;
        in = new Scanner(System.in);
    }

    void out(Object msg) {
        out.accept(msg);
    }

    void printEmptyLine() {
        out("\n");
    }

    void printPlayerMoveInfo() {
        printEmptyLine();
        out("Available moves:\n\n");
        out("1. Discard a pair\n");
        out("2. Pick a card from previous player\n");
        out("3. Leave the game\n");
        printEmptyLine();
    }

    void printWelcomeMessage() {
        out("Welcome to Black Witch!\n".toUpperCase());
        out("Please take a few minutes to set up the game.\n\n");
    }

    void printPlayersHaveCardsMsg() {
        System.out.println("All players have been dealt their cards.\n\n");
    }

    void printPlayerWinsMsg(int playerId) {
        out("Player " + playerId + " WINS!\n");
    }

    void printPlayerLostMsg(int playerId) {
        out("Player " + playerId + " LOST!\n");
    }

    int readNumberOfPlayers() {
        int numberOfPlayers;
        numberOfPlayers = readNumber("How many players wish to participate?: ");

        while (numberOfPlayers < 2) {
            numberOfPlayers = readNumber("A minimum of 2 players is required. Please try again:  ");
        }

        return numberOfPlayers;
    }

    int readNumberOfCardsInHand() {
        String changeAmountOfCardsToDeal = readText("By default a player receives 5 cards. Would you like to change the amount? (y/n): ");
        if (changeAmountOfCardsToDeal.equals("y")) {
            return readNumberOfCards();
        } else {
            return DEFAULT_NUMBER_OF_CARDS;
        }
    }

    int readNumberOfCards() {
        final String readNumberOfCardsMsg = "Specify the number of cards (only odd number of cards is allowed): ";
        int numberOfCardsInHand = readNumber(readNumberOfCardsMsg);

        while (numberOfCardsInHand % 2 == 0) {
            numberOfCardsInHand = readNumber("You have chosen an incorrect number of cards to play with. Please try again:  ");
        }

        return numberOfCardsInHand;
    }

    String readText(String msg) {
        String text = "";
        boolean isText = false;

        while (!isText) {
            out(msg);
            text = in.next();

            if (text.matches(IS_NUMBER_PATTERN)) {
                out("No numbers allowed. Try again. \n\n");
            } else {
                isText = true;
            }
        }

        return text;
    }

    int readNumber(String msg) {
        int number = 0;
        boolean isNumber = false;

        while (!isNumber) {
            out(msg);
            String text = in.next();

            if (text.matches(IS_NUMBER_PATTERN)) {
                number = Integer.parseInt(text);
                isNumber = true;
            } else {
                out("This is not an number! Try again.\n\n");
            }
        }

        return number;
    }
}
