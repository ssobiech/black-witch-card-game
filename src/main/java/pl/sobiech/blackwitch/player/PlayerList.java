package pl.sobiech.blackwitch.player;

import pl.sobiech.blackwitch.card.Card;
import pl.sobiech.blackwitch.card.CardDealer;

import java.util.*;

public class PlayerList implements ListIterator<Player> {

    private final CardDealer cardDealer;
    private List<Player> players;
    private int cursor;
    private int currentElement;

    public PlayerList(int numberOfPlayers, int numberOfCardsInHand) {
        this.cardDealer = new CardDealer();
        cardDealer.shuffleCards();
        createPlayers(numberOfPlayers, numberOfCardsInHand);
    }

    private void createPlayers(int numberOfPlayers, int numberOfCardsInHand) {
        players = new ArrayList<>();

        for (int i = 0; i < numberOfPlayers; i++) {
            List<Card> cards = cardDealer.dealCards(numberOfCardsInHand);
            Player player = new Player(i + 1, cards);
            players.add(player);
        }
    }

    public int getQuantityOfPlayers() {
        return players.size();
    }

    @Override
    public boolean hasNext() {
        return !players.isEmpty();
    }

    @Override
    public Player next() {
        int i = this.cursor;

        if (this.cursor >= players.size() || this.cursor < 0) {
            this.cursor = 0;
            i = this.cursor;
        }

        this.cursor++;
        this.currentElement = i;

        return this.players.get(i);
    }

    @Override
    public boolean hasPrevious() {
        return this.cursor > 0;
    }

    @Override
    public Player previous() {
        int i = this.currentElement - 1;

        if (i < 0) {
            i = players.size() - 1;
        }

        return players.get(i);
    }

    @Override
    public int nextIndex() {
        return currentElement + 1;
    }

    @Override
    public int previousIndex() {
        return currentElement - 1;
    }

    @Override
    public void remove() {
        players.remove(currentElement);
        incrementCursorIfLastIndexBiggerThanCurrentElement();
    }

    public void remove(Player player) {
        players.remove(player);
        incrementCursorIfLastIndexBiggerThanCurrentElement();
    }

    private void incrementCursorIfLastIndexBiggerThanCurrentElement() {
        int lastIndex = players.size() - 1;
        if(currentElement < lastIndex) {
            this.cursor++;
        }
    }

    @Override
    public void set(Player cards) {
        this.players.listIterator().set(cards);
    }

    @Override
    public void add(Player cards) {
        this.players.listIterator().add(cards);
    }
}
