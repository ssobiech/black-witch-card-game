package pl.sobiech.blackwitch.player;

import pl.sobiech.blackwitch.card.Card;

import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

public class Player implements Iterable<Card> {

    private Integer id;
    private List<Card> cards;

    Player(Integer id, List<Card> cards) {
        this.id = id;
        this.cards = cards;
    }

    public Integer getId() {
        return id;
    }

    public boolean hasCards() {
        return !cards.isEmpty();
    }

    public int getQuantityOfCards() {
        return cards.size();
    }

    public Card getCard(int cardIndex) {
        return cards.get(cardIndex);
    }

    public void addCard(Card card) {
        cards.add(card);
    }

    public void removeCard(int cardIndex) {
        Card card = cards.get(cardIndex);
        cards.remove(card);
    }

    public void displayCards() {
        this.forEach(card -> System.out.print(card + " "));
        System.out.println();
    }

    public boolean discardPair(Card firstCard, Card secondCard) {
        boolean isPairRemoved = false;

        if (firstCard.compareTo(secondCard) > 0) {
            cards.remove(firstCard);
            cards.remove(secondCard);
            isPairRemoved = true;
        } else {
            System.out.println("That is not a pair!");
        }

        return isPairRemoved;
    }

    @Override
    public Iterator<Card> iterator() {
        return cards.iterator();
    }

    @Override
    public void forEach(Consumer<? super Card> action) {
        cards.forEach(action);
    }

    @Override
    public String toString() {
        StringBuilder player = new StringBuilder("PLAYER " + this.id + "\n\n");
        this.forEach(card -> {
            player.append(card);
            player.append(" ");
        });

        return player.toString();
    }
}
